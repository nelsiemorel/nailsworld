<?php
    /* Accès à la table produits de la base de données *************************/
    class Contact {

        // Connection
        private $cnx;
        // Colonne
        public $id;
public $nom;
public $email;
public $msgboite;

        // Connexion à la base de données
        public function __construct(array $donnees){
            $this->hydrate($donnees);
        }

        public function hydrate(array $donnees){
            foreach ($donnees as $key => $value){
                // On récupère le nom du setter correspondant à l'attribut.
                $method = 'set'.ucfirst($key);
    
                // Si le setter correspondant existe.
                if (method_exists($this, $method)){
                    // On appelle le setter.
                    $this->$method($value);
                }
            }
        }
    
        // Getters
        public function getId()     {return $this->id;}
        public function getNom(){return $this->nom;}
        public function getEmail(){ return $this->email;}
        public function getMessage(){ return $this->msgboite;}

        // Setters
        public function setId($id)      {$this->id      = $id;}

    }
