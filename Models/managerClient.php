<?php
    require_once("db.php");
    /* Accès à la table client de la base de données *************************/
    class managerClient extends Database {

        // Colonne
        public $id;
        public $name;
        public $email;
        public $phone;

        // Connexion à la base de données
        public function __construct()
        {
            parent::__construct();
        }

        // // Extraction des données des clients depuis la base de données.
        // public function getClients()
        // {
        //     $sql = "SELECT * FROM client;";
        //     $rqt = $this->cnx->prepare($sql);
        //     $rqt->execute();
        //     $clients = $rqt->fetchAll(PDO::FETCH_ASSOC);
        //     $rqt->closeCursor(); // Achève le traitement de la requête
        //     return $clients;
        // }

        // // Récupère les données d'un client déterminé par son id
        // public function getClient($id)
        // {
        //     $sql = "SELECT * FROM client WHERE id = ?;";
        //     $rqt = $this->cnx->prepare($sql);
        //     $rqt->execute(array($id));
        //     $client = $rqt->fetch();
        //     $rqt->closeCursor();
        //     return $client;
        // }

        // // Supprime les données d'un client déterminé par son id
        // public function deleteClient($id)
        // {
        //     $sql = "DELETE FROM client WHERE id = ?;";
        //     $rqt = $this->cnx->prepare($sql);
        //     $resultat = $rqt->execute(array($id));
        //     return $resultat;
        // }

        // // Ajoute un client
        // public function addClient(array $client)
        // {

        //     $sql = "INSERT INTO client(name, email, phone) VALUES(?,?,?)";
        //     $rqt = $this->cnx->prepare($sql);
        //     $resultat  = $rqt->execute(array($client[0], $client[1], $client[2]));
        //     return $resultat;
        // }

        // // Modifie un client
        // public function updateClient(array $client)
        // {
        //     var_dump($client);
        //     exit;
        //     $sql = "UPDATE client SET name = ?, email = ?, phone = ? WHERE id = ?";
        //     $rqt = $this->cnx->prepare($sql);
        //     $resultat  = $rqt->execute(array($client[0], $client[1], $client[2], $client[3]));
        //     return $resultat;
        // }
    }
?>
