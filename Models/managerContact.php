<?php
require_once("db.php");
/* Accès à la table client de la base de données *************************/
class manageContact extends Database
{

    // Colonne
    public $id;
    public $nom;
    public $email;
    public $msgboite;

    // Connexion à la base de données
    public function __construct()
    {
        parent::__construct();
    }

    public function getContact()
    {
        $sql = "SELECT * FROM contact";
        $rqt = $this->cnx->prepare($sql);
        $rqt->execute();
        $membres = $rqt->fetchAll(PDO::FETCH_ASSOC);
        $rqt->closeCursor(); // Achève le traitement de la requête
        return $membres;
    }

    public function Insert()
    {
        if (isset($_POST['contact'])) {
            $nom = $_POST['nom'];
            $email = $_POST['email'];
            $msgboite = $_POST['message'];

            $sql = "INSERT INTO contact VALUES('','$nom','$email','$msgboite')";
            $rqt = $this->cnx->prepare($sql);
            $rqt->execute();
            $membres = $rqt->fetchAll(PDO::FETCH_ASSOC);
            $rqt->closeCursor(); // Achève le traitement de la requête
            return $membres;
        }
    }
}
