<?php
    /* Accès à la table produits de la base de données *************************/
    class Membres {

        // Connection
        private $cnx;
        // Colonne
        // Colonne
        public $id;
        public $pseudo;
        public $mdp;
        public $nom;
        public $prenom;
        public $email;
        public $ville;
        public $codepostal;
        public $adresse;

        // Connexion à la base de données
        public function __construct(array $donnees){
            $this->hydrate($donnees);
        }

        public function hydrate(array $donnees){
            foreach ($donnees as $key => $value){
                // On récupère le nom du setter correspondant à l'attribut.
                $method = 'set'.ucfirst($key);
    
                // Si le setter correspondant existe.
                if (method_exists($this, $method)){
                    // On appelle le setter.
                    $this->$method($value);
                }
            }
        }
    
        // Getters
        public function getId()     {return $this->id;}
        public function getPseudo()     {return $this->pseudo;}
        public function getmdp()   {return $this->mdp;}
        public function getnom()  {return $this->nom;}
        public function getprenom()  {return $this->prenom;} 
        public function getemail()  {return $this->email;} 
        public function getville()  {return $this->ville;} 
        public function getcodepostal()  {return $this->codepostal;}
        public function getadresse()  {return $this->adresse;}  

        // Setters
        public function setID($id)      {$this->id     = $id;}
        public function setPseudo($pseudo)      {$this->pseudo     = $pseudo;}
        public function setmdp($mdp)  {$this->mdp  = $mdp;}
        public function setnom($nom){$this->nom  = $nom;}
        public function setprenom($prenom){$this->prenom   = $prenom;}
        public function setemail($email){$this->email  = $email;}
        public function setville($ville){$this->ville  = $ville;}
        public function setcodepostal($codepostal){$this->codepostal  = $codepostal;}
        public function setadresse($adresse){$this->adresse  = $adresse;}

    }
?>