<?php
    /* Accès à la table produits de la base de données *************************/
    class Produits {

        // Connection
        private $cnx;
        // Colonne
        public $id;
        public $reference;
        public $name;
        public $description;
        public $categorie;
        public $price;
        public $photo;

        // Connexion à la base de données
        public function __construct(array $donnees){
            $this->hydrate($donnees);
        }

        public function hydrate(array $donnees){
            foreach ($donnees as $key => $value){
                // On récupère le nom du setter correspondant à l'attribut.
                $method = 'set'.ucfirst($key);
    
                // Si le setter correspondant existe.
                if (method_exists($this, $method)){
                    // On appelle le setter.
                    $this->$method($value);
                }
            }
        }
    
        // Getters
        public function getId()     {return $this->id;}
        public function getReference()   {return $this->reference;}
        public function getName()  {return $this->name;}
        public function getDescription()  {return $this->description;} 
        public function getCategories()  {return $this->categorie;} 
        public function getPrice()  {return $this->price;} 
        public function getPhoto()  {return $this->photo;} 

        // Setters
        public function setId($id)      {$this->id      = $id;}
        public function setRef($reference)  {$this->reference   = $reference;}
        public function setName($name){$this->name   = $name;}
        public function setDescription($description){$this->description   = $description;}
        public function setCategories($categorie){$this->categorie  = $categorie;}
        public function setPrice($price){$this->price  = $price;}
        public function setPhoto($photo){$this->photo  = $photo;}

    }
