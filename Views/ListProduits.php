<?php
require_once("Models/produits.php");
$titre = "Produits";

if(isset($_GET['idcat'])){
    $idcat = $_GET['idcat'];
    $produits = $this->manageproduits->getProductCat($idcat);
}
$contenu = "";
    foreach ($produits as $cle => $ligne) {
        $produits = new Produits($ligne);
        $contenu .= '
        <div class="container mt-3" >
        <div class="col-lg-3 col-md-6 offset-md-0 offset-sm-1 col-sm-10 offset-sm-1 my-lg-0 my-2 float-left">
        <a href="index.php?action=FicheProduits&amp;id='.$produits->getId().'" style="text-decoration: none;color:black;">
            <div class="card"> <img class="card-img-top" height="280" src="' . $produits->getPhoto() . '">
                <div class="card-body">
                    <div class="d-flex align-items-start justify-content-between">
                        <div class="d-flex flex-column">
                            <div class="h6 font-weight-bold">' . $produits->getName() . '</div>
                            <div class="text-muted">' . $produits->getPrice() . '€</div>
                        </div>
                        <div class="btn"><span class="far fa-heart"></span></div>
                    </div>
                </div>
            </div>
        </a>
        </div>
</div>
           ';
    }
include "template.php";
?>