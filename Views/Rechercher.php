<?php
$titre = "Rechercher";
if(isset($_POST['rechercher'])){
    $name = $_POST['nom'];
    if(!empty($name)){
        $tbl = $this->manageproduits->GetProd($name);
        foreach ($tbl as $tblprod){
           $contenu='        <div class="container mt-3" >
            <div class="col-lg-3 col-md-6 offset-md-0 offset-sm-1 col-sm-10 offset-sm-1 my-lg-0 my-2 float-left">
            <a href="index.php?action=FicheProduits&amp;id='.$tblprod['id'].'" style="text-decoration: none;color:black;">
                <div class="card"> <img class="card-img-top" height="280" src="' .$tblprod['photo']. '">
                    <div class="card-body">
                        <div class="d-flex align-items-start justify-content-between">
                            <div class="d-flex flex-column">
                                <div class="h6 font-weight-bold">'.$tblprod['name']. '</div>
                                <div class="text-muted">' .$tblprod['price']. '€</div>
                            </div>
                            <div class="btn"><span class="far fa-heart"></span></div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
    </div>';
        }
    }
}
include "template.php";

?>