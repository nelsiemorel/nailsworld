<?php
require_once("Models/produits.php");
$titre = " Fiche Produits";


if(isset($_GET['id'])){
    $id = $_GET['id'];
    $produits = $this->manageproduits->getProduitsID($id);
}

foreach ($produits as $cle => $ligne) {
    $produits = new Produits($ligne);
    $contenu = '
    <div class="codepen-container">
    <div class="content-container">
      <div class="left-container">
        <div class="triangle-topleft"><a href="index.php?action=produitcat&idcat=1">
          <div class="back-arrow" id="buy-toaster"></div></a>
        </div>
        <div class="product-image--container">
          <img class="product-image--featured" width="300" id="featured" src="'.$produits->getPhoto().'" alt="toaster"/>
        </div>
      </div>
      <div class="right-container">
        <div>
          <h1 class="title">'.$produits->getName().'</h1>
        </div>
        <span>
          <p>Prix: 
            <span class="emphasize">'.$produits->getPrice().'€</span>
          </p>
          <label for="quantity">Quantité:</label>
          <select name="quantity" class="select-dropdown">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
          </select>
        </span>
        <div>
          <h2 class="title">Description</h2>
          <p>'.$produits->getDescription().'</p>
        </div>
        <div>
          <button class="my-btn flex-btn">
            <span>
              <img src="https://cdn1.iconfinder.com/data/icons/material-core/20/shopping-cart-128.png" class="cart-icon"/>
            </span>
            <span class="btn-text"><a href="index.php?action=Panier">Ajouter</a>
            </span>
          </button>
        </div>
      </div>
    </div>
  </div>
       ';
}

include "template.php";
?>


<style>
body {
  background-color: #EAEAEA;
  font-family: 'Verdana', sans-serif;
}

.codepen-container {
  padding: 3em;
}

.content-container {
  display: flex;
  margin: 0 auto;
  background-color: white;
  max-width: 660px;
  width: 100%;
}

.product-image--container {
  margin: 0 2em;
}

.triangle-topleft {
  position: relative;
	width: 0;
	height: 0;
	border-top: 100px solid #FFC500;
	border-right: 100px solid transparent;
  -webkit-transition: all ease 0.25s;
  transition: all ease 0.25s;
}

.triangle-topleft:hover {
  border-top: 100px solid #ffd54d;
  -webkit-transition: all ease 0.25s;
  transition: all ease 0.25s;
}

.back-arrow {
  cursor: pointer;
  position: absolute;
  top: -90px;
  left: 15px;
  background-image: url('https://cdn2.iconfinder.com/data/icons/freecns-cumulus/16/519627-127_ArrowLeft-128.png');
  background-size: 35px 35px;
  background-repeat: no-repeat;
  width: 40px;
  height: 40px;
}



.product-image--list li {
  margin: 0 5px;
  border: 2px solid #eaeaea;
  border-radius: 5px;
  -webkit-transition: all ease 0.25s;
  transition: all ease 0.25s;
}

.product-image--list li.item-selected {
  box-shadow: 1px 1px 1px #888888;
  border: 2px solid #FFC500;
  -webkit-transition: all ease 0.25s;
  transition: all ease 0.25s;
}

.product-image--item {
  max-width: 40px;
  max-height: 40px;
}

.right-container {
  margin: 1em;
}

.title {
  margin: 0.5em 0 0 0;
}

.subtitle {
  font-size: 1.25em;
}

.subtitle-container {
  margin: 0.5em 0 0.5em 0;
  opacity: 0.6;
}

// RATING STARS FROM https://www.everythingfrontend.com/posts/star-rating-input-pure-css.html
.rating {
    overflow: hidden;
    display: inline-block;
    font-size: 0;
    position: relative;
}
.rating-input {
    float: right;
    width: 16px;
    height: 16px;
    padding: 0;
    margin: 0 0 0 -16px;
    opacity: 0;
}
.rating:hover .rating-star:hover,
.rating:hover .rating-star:hover ~ .rating-star,
.rating-input:checked ~ .rating-star {
    background-position: 0 0;
}
.rating-star,
.rating:hover .rating-star {
    position: relative;
    float: right;
    display: block;
    width: 16px;
    height: 16px;
    background: url('http://kubyshkin.ru/samples/star-rating/star.png') 0 -16px;
}

.reviews {
  text-decoration: none;
}

a:hover.reviews {
  color: #365afc;
  text-decoration: underline;
}

.emphasize {
  color: #b32600;
  font-size: 1.5em;
  letter-spacing: -2px;
}

.select-dropdown {
  max-width: 50px;
  width: 100%;
}

.my-btn {
  background-color: #FFC500;
  border-radius: 5px;
  max-width: 275px;
  width: 100%;
  height: 50px;
  -webkit-transition: all ease 0.25s;
  transition: all ease 0.25s;
}

.my-btn:hover {
  box-shadow: 1px 1px 1px #888888;
  background-color: #ffd54d;
  -webkit-transition: all ease 0.25s;
  transition: all ease 0.25s;
}

.flex-btn {
  display: flex;
  align-items: center;
}

.cart-icon {
  margin: 2px;
  width: 30px;
  height: 30px;
}

.btn-text {
  font-weight: bold;
  font-size: 1.5em;
  margin: 0 auto;
}</style>
